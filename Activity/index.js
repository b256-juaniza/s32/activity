const http = require("http");

let port = 4000;

let courses = {

		"Front_End_Development" : ["HTML", " CSS", " Bootstrap"],
		"Back_End_Development" : ["JavaScript", " JSON ", " MongoDB "],
		"New_Programming_Courses" : ['Java']
	}

let listCourseFront = "";
let listCourseBack = "";
let newCourses = "";

courses.Front_End_Development.forEach(function(courseEnd){
	listCourseFront += " " + courseEnd + ",";
});

courses.Back_End_Development.forEach(function(courseBack){
	listCourseBack += " " + courseBack + ",";
});

courses.New_Programming_Courses.forEach(function(newCourse){
	newCourses += " " + newCourse + ",";
});

http.createServer(function(req, res) {
	if (req.url == "/" && req.method == "GET") {
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to Booking System!");
	}

	if (req.url == "/profile" && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Welcome to your Profile!");
	}

	if (req.url == "/courses" && req.method == "GET") {
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.write("Here's our Courses Available!\n");
		res.write(`Front End Development - ${courses.Front_End_Development} \n`);
		res.write(`Back End Development - ${courses.Back_End_Development} \n`);
		res.write(`New Courses - ${courses.New_Programming_Courses} \n`);
		res.end();
	}

	if (req.url == "/addcourse" && req.method == "POST") {
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.write("Add a Course to our Resources\n");
		courses = JSON.stringify(courses);

		let addCourse = " ";

		req.on("data", function(data){
			addCourse += data;
		})

		req.on("end", function(){
		courses = JSON.parse(courses);
		courses.New_Programming_Courses.push(addCourse);
		console.log(courses);
		res.write("Currently Available Courses: \n")
		res.write(`Front End Development - ${courses.Front_End_Development} \n`);
		res.write(`Back End Development - ${courses.Back_End_Development} \n`);
		res.write(`New Courses - ${courses.New_Programming_Courses} \n`);
		res.end();
		})
	}

	if (req.url == '/updateCourse' && req.method == "PUT"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.write("Update a Course to our Resources");
		res.end();
	}

	if (req.url == '/archiveCourse' && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.write("Archive Courses to our Resources");
		res.end();
	}

}).listen(port);

console.log(`Server is running at localhost:4000`);

