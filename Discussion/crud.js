const http = require("http");

const port = 4000;

//Mock Database 
let directory = [
		{
			"name" : "Brandon",
			"email" : "brandon@gmail.com"
		},
		{
			"name" : "Robert",
			"email" : "robert@gmail.com"
		}
	]

const server = http.createServer(function(req, res){
	if (req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
	}

	if (req.url == "/users" && req.method == "POST"){

		let requestbody = "";

		req.on("data", function(data){
			requestbody += data;
		})

		req.on("end", function(){
			console.log(typeof requestbody);

			// requestbody = "{'email' : 'object1' }"

			requestbody = JSON.parse(requestbody);

			// requestbody = {'email' : 'object1' }

			let newUser = {
				'name' : requestbody.name,
				'email' : requestbody.email
			}
 
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);